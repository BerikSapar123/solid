import java.util.*;
public class Aquarium {
	private ArrayList<Reptile> reptiles;
	private ArrayList<Fish> fish;
	private int accessoriesPrice = 0;
	public Aquarium(int price){
		accessoriesPrice = price;
		reptiles = new ArrayList<Reptile>();
		fish = new ArrayList<Fish>();
	}
	public void addReptile(Reptile reptile){
		reptiles.add(reptile);
	}
	public void addFish(Fish fish){
		this.fish.add(fish);
	}
	public int getTotalPrice(){
		int sum = accessoriesPrice;
		for (Reptile a : reptiles)
			sum += a.getPrice();
		for (Fish a : fish)
			sum += a.getPrice();
		return sum;
	}
	public int getNumberOfAnimals(){
		return reptiles.size() + fish.size();
	}
}
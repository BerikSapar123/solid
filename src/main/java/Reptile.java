public abstract class Reptile implements Animal {
    private int numberOfLegs;
    private int price;
    public Reptile(int legs, int price){
        numberOfLegs = legs;
        this.price = price;
    }
    public int getPrice(){
    	return price;
    }
    public int getLegs(){
        return numberOfLegs;
    }
}

class Crocodile extends Reptile {
    public Crocodile(int price) {
        super(4, price);
    }

    @Override
    public void eat(String food) {
        System.out.println("Crocodile ate a " + food + " today");
    }

    @Override
    public void move(String direction) {
        System.out.println("Crocodile moved to the " + direction);
    }

    @Override
    public String info() {
        return "This is a crocodile having " + getLegs() + " legs";
    }
}

class Snake extends Reptile {
    public Snake(int price) {
        super(0, price);
    }

    @Override
    public void eat(String food) {
        System.out.println("Snake ate a " + food + " today");
    }

    @Override
    public void move(String direction) {
        System.out.println("Snake moved to the " + direction);
    }

    @Override
    public String info() {
        return "This is a snake having " + getLegs() + " legs that costs " + getPrice() + " dollars";
    }
}
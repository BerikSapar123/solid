public class Main {
    public static void main(String[] args) {
        Reptile croc = new Crocodile(1000);
        Animal goldenFish = new GoldenFish(90);
        Aquarium aquarium = new Aquarium(499);
        aquarium.addReptile(new Snake(1000));
        aquarium.addFish(new RainbowFish(33));
        aquarium.addReptile((Reptile)croc);
        aquarium.addFish((Fish)goldenFish);
        croc.eat("fish");
        croc.eat("zebra");
        goldenFish.eat("seaweed");
        croc.move("swamp");
        System.out.println(croc.info());
        System.out.println(goldenFish.info());
        System.out.println("Crocodile has " + croc.getLegs() + " legs");
        System.out.println("There are " +  aquarium.getNumberOfAnimals() + " animals in the aquarium");
        System.out.println("Price of aquarium accessories and animals: " + aquarium.getTotalPrice());
    }
}
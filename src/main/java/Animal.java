public interface Animal {
    void eat(String food);
    void move(String direction);
    String info();
}

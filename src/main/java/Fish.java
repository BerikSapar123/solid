public abstract class Fish implements Animal {
    private int numberOfFins;
    private int price;
    public Fish(int fins, int price){
        numberOfFins = fins;
        this.price = price;
    }
    public int getPrice(){
    	return price;
    }
    public int getNumberOfFins() {
        return numberOfFins;
    }
}

class GoldenFish extends Fish {
    public GoldenFish(int price) {
        super(7, price);
    }
    @Override
    public void eat(String food) {
        System.out.println("Golden fish ate " + food);
    }
    @Override
    public void move(String direction) {
        System.out.println("Golden fish moved to the " + direction);
    }
    @Override
    public String info() {
        return "This is a Golden Fish having " + getNumberOfFins() + " fins";
    }
}

class RainbowFish extends Fish {
    public RainbowFish(int price) {
        super(4, price);
    }
    @Override
    public void eat(String food) {
        System.out.println("Rainbow fish ate " + food);
    }
    @Override
    public void move(String direction) {
        System.out.println("Rainbow fish moved to the " + direction);
    }
    @Override
    public String info() {
        return "This is a Rainbow Fish having " + getNumberOfFins() + " fins that costs " + getPrice() + " dollars";
    }
}
